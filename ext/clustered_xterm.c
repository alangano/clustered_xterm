#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/extensions/XTest.h>
#include <unistd.h>

/*
#include <X11/keysymdef.h>
#include <stdlib.h>
#include <X11/Xutil.h>
#include <X11/Intrinsic.h>
#include <X11/Xresource.h>
#include <string.h>
*/

//#define KEYCODE XK_Down
#define KEYCODE XK_A
//#define KEYCODE 38

//-----------------------------------------------------------------------------
static Display* g_display = NULL;

//-----------------------------------------------------------------------------
XKeyEvent ClusteredXTerm_create_key_event(
   Window   i_win,
   Window   i_win_root,
   int      i_press,
   int      i_keycode,
   int      i_modifiers
)
{
   XKeyEvent event;

   event.display     = g_display;
   event.window      = i_win;
   event.root        = i_win_root;
   event.subwindow   = None;
   event.time        = CurrentTime;
   event.x           = 1;
   event.y           = 1;
   event.x_root      = 1;
   event.y_root      = 1;
   event.same_screen = True;
   event.keycode     = XKeysymToKeycode(g_display,i_keycode);
   event.state       = i_modifiers;

   if(i_press)
      event.type = KeyPress;
   else
      event.type = KeyRelease;

   return event;
}

//-----------------------------------------------------------------------------
int ClusteredXTerm_send_keypress(int i_win_id, int i_keycode, int i_mods)
{
   if(g_display == NULL) return -1;
   //Display* t_display = g_display;
   //if(t_display == NULL) return -2;

   Window t_win_root = XDefaultRootWindow(g_display);

   //printf("keycode: %d\n",i_keycode);

   //printf("window: %d\n",i_win_id);
   XKeyEvent t_event =
      ClusteredXTerm_create_key_event(
         (Window)i_win_id,
         t_win_root,
         1,
         i_keycode,
         i_mods
      );

   XSendEvent(
      g_display,
      (Window)i_win_id,
      True,
      KeyPressMask,
      (XEvent*)&t_event
   );

   XFlush(g_display);

   return 0;
}

//-----------------------------------------------------------------------------
int ClusteredXTerm_send_keyrelease(int i_win_id, int i_keycode)
{
   if(g_display == NULL) return -1;
   //Display* t_display = g_display;
   //if(t_display == NULL) return -2;

   Window t_win_root = XDefaultRootWindow(g_display);

   //printf("keycode: %d\n",i_keycode);

   //printf("window: %d\n",i_win_id);
   XKeyEvent t_event =
      ClusteredXTerm_create_key_event(
         (Window)i_win_id,
         t_win_root,
         0,
         i_keycode,
         0
      );

   XSendEvent(
      g_display,
      (Window)i_win_id,
      True,
      KeyPressMask,
      (XEvent*)&t_event
   );

   XFlush(g_display);

   return 0;
}

//-----------------------------------------------------------------------------
int ClusteredXTerm_open_x_display(void)
{
   g_display = NULL;
   Display* t_display = XOpenDisplay(0);
   if(t_display == NULL) return -1;
   g_display = t_display;
   return(0);
}
int ClusteredXTerm_close_x_display(void)
{
   if(g_display == NULL) return(-1);
   XCloseDisplay(g_display);
   return(0);
}

//-----------------------------------------------------------------------------
int main(void)
{
   //int t_win_id[2] = {163577870,167772174};
//   int t_win_id[2] = {77594638};
   ClusteredXTerm_open_x_display();
   int t_win_id = 100663310;
   ClusteredXTerm_send_keypress(t_win_id,44,2);
   return(0);
}

//-----------------------------------------------------------------------------


