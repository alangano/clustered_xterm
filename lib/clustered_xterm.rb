
$VERBOSE = true
require 'pathname'
require 'logger'
require 'date'
require 'ffi'
require 'ostruct'

class Logger
   attr_accessor :last_date
end
LOGGER = Logger.new(STDERR)
LOGGER.formatter = proc { |sev,time,progname,msg|
   (
      "#{sev[0]} (#{Process.pid}) " +
      "#{time.strftime('%H:%M:%S')}: #{msg}\n"
   ).tap { |m|
      Date.today.tap { |d|
         if logger.last_date != d
            $stderr.puts "DATE: #{d}"
            logger.last_date = d
         end
      }
   }
}

module Kernel
   def logger
      LOGGER
   end
end


#------------------------------------------------------------------------------
$VERBOSE = false
require 'tk'
$VERBOSE = true
$VERBOSE = false # I Guess

#------------------------------------------------------------------------------

module CXLIB
   extend FFI::Library
   ffi_lib "#{File.dirname(__FILE__)}/../ext/clustered_xterm.so"
   attach_function :open_x_display,:ClusteredXTerm_open_x_display, [],:int
   attach_function :main,:ClusteredXTerm_main, [],:int
   attach_function :send_keypress,
      :ClusteredXTerm_send_keypress,
      [:int,:int,:int],
      :int
   attach_function :send_keyrelease,
      :ClusteredXTerm_send_keyrelease,
      [:int,:int],
      :int
end

class XTerm
   def self.start(i_control,i_options)
      t_xterm_args = [
         "#{i_options.xterm_args}",
         "-font #{i_options.font}",
         "-bg #{i_options.bg}",
         "-fg #{i_options.fg}",
         "-T '#{i_options.title}'",
         "-xrm 'xterm*allowSendEvents:True'",
      ].join(' ')

      i_options.command and t_xterm_args << " -e #{i_options.command}"

      logger.debug "xterm-args: #{t_xterm_args}"

      ENV["CLUSTERED_XTERM_WINDOW"] = "YES"
      i_options.env_vars.each { |n,v| ENV["#{n}"] = "#{v}" }

      if t_pid = fork
         logger.debug "child-pid: #{t_pid}"

         i_options.env_vars.each { |n,v| ENV.delete("#{n}") }

         t_children_file = Pathname.new("/proc/#{t_pid}/task/#{t_pid}/children")
         10.times {
            t_children_file.exist? and
               t_children_file.read.size > 0 and
               break
            logger.debug 'sleeping'
            sleep 0.5
         }
         t_child_pids =
            t_children_file.read.split(' ').collect { |pid| pid.to_i }
         t_child_pids.count == 0 and raise "child bash process not found!"
         t_child_pids.count > 1 and
            raise "more than one child found -- should not happen, right?"

         t_shell_pid = t_child_pids.first
         logger.debug "shell-pid: #{t_shell_pid}"

         t_line =
            File.read("/proc/#{t_shell_pid}/environ").split(/\0/).
               find { |line| line =~ /\AWINDOWID=\d+\Z/ }
         if t_line.nil? || t_line.size == 0
            raise "WINDOWID not found in shell env"
         end

         t_win_id = t_line.sub(/\AWINDOWID=(\d+)\Z/,'\1').to_i

         XTerm.new(i_control,t_pid,t_shell_pid,t_win_id,i_options)
      else
         begin
            exec("/usr/bin/xterm #{t_xterm_args}")
         ensure
            exit!(9)
         end
      end
   end
   attr_accessor :pid, :shell_pid, :win_id, :options
   attr_accessor :active, :menu, :menu_index
   attr_accessor :control
   def initialize(i_control,i_pid,i_shell_pid,i_win_id,i_options)
      self.control   = i_control
      self.pid       = i_pid
      self.shell_pid = i_shell_pid
      self.win_id    = i_win_id
      self.options   = i_options
      start_threaded_wait
      @dead = false
      @cleaned = false
      @active = true
   end
   def send_keypress(i_keycode,i_mods)
      active or return
      CXLIB.send_keypress(win_id,i_keycode,i_mods)
   end
   def send_keyrelease(i_keycode)
      active or return
      CXLIB.send_keyrelease(win_id,i_keycode)
   end
   def close
      Process.kill(2,pid)
   end
   def proc_dir
      @proc_dir ||= Pathname.new("/proc/#{pid}")
   end
   def start_threaded_wait
      @wait_thread =
         Thread.new {
            Process.wait(pid)
            logger.info 'xterm has closed'
            begin
               control.tkmenu.delete(menu_index)
            rescue Exception
            end
            @dead = true
         }
   end
   def dead?
      @dead
   end
   def cleanup!
      @wait_thread.join
      logger.info 'dead xterm has been cleaned up'
      @cleaned = true
   end
   def cleaned?
      @cleaned
   end
end

class ClusteredXTerm
   SHIFT_CHARS = [
      '!', '"', '#', '$', '%', '&', '(', ')', '*', '+', ':', '>', '?', '@',
      'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O',
      'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '^', '_',
      '{', '|', '}', '~',
   ]
   attr_accessor :global_options, :windows
   attr_accessor :xterms
   attr_accessor :key_mods
   attr_accessor :tkroot, :tkmenu
   def initialize(*i_windows,i_global:)
      self.global_options = {
         font: '-*-tixus-*-*-*-*-*-*-*-*-*-*-*-*',
         bg: 'white',
         fg: 'darkgreen',
         env_vars: {},
         command: nil,
         title: 'ClusteredXTerm',
      }.merge(i_global)

      self.windows =
         i_windows.collect { |w| OpenStruct.new(global_options.merge(w)) }

#      puts '-'*79
#      puts global_options.inspect
#      puts '-'*79
#      windows.collect { |w| puts w.inspect }
#      puts '-'*79

      self.xterms = []
      self.key_mods = 0
   end
   def run
      windows.each { |w| xterms << XTerm.start(self,w) }
      logger.debug "open_x_display(): #{CXLIB.open_x_display}"
      start_input_window
   end
   def xterm_menu_reverse
      xterms.each { |x|
         x.menu.invoke(x.menu_index)
      }
   end
   def xterm_menu_click(i_xterm)
      i_xterm.active =
         if i_xterm.active
            false
         else
            true
         end
   end
   def start_input_window
      self.tkroot = Tk::Root.new {
         title 'Clustered XTerm'
      }

      self.tkmenu = TkMenu.new(tkroot)

      tkmenu.add_command(label:'Reverse',command: proc {xterm_menu_reverse} )
      tkmenu.add_separator

      xterms.each.with_index { |x,i|
         t_label = "#{x.options.title}(#{i})"
         x.menu =
            tkmenu.add_checkbutton(
               label:      t_label,
               onvalue:    true,
               offvalue:   false,
               command:    proc { xterm_menu_click(x) }
            )
         x.menu_index = i+3            # menu-item offset
         x.menu.invoke(x.menu_index)   # turn on all checks (initial state)
         x.active = true               # set initial state to match checkboxes
      }

      t_menu_bar = TkMenu.new
      t_menu_bar.add(
         'cascade',
         'menu'  => tkmenu,
         'label' => "XTerms"
      )
      tkroot.menu(t_menu_bar)



      tkroot.protocol("WM_DELETE_WINDOW") { handle_delete_window }

      #Tk::Label.new(tkroot) do
      #   text 'Clustered XTerm'
      #   pack { padx 15 ; pady 15; side 'left' }
      #end

      Tk::Text.new(tkroot) {
         #width 40
         #height 1
         pack { padx 15 ; pady 15; side 'left' }
      }.tap { |t|
         t.bind('KeyPress') { |e| handle_key_press(e) }
         t.bind('KeyRelease') { |e| handle_key_release(e) }
         t.bind('ButtonRelease-2') { |e| handle_button_2_release(e) }

         TkTimer.new(500, -1, proc{ t.clear }).start
      }

      Tk.mainloop
   end
   def handle_key_press(e)
      #puts '-'*79
      #puts e.inspect
      #puts '-'*79

      case e.keysym
         when 'Shift_L' then self.key_mods = key_mods | 1
         when 'Shift_R' then self.key_mods = key_mods | 1
         when 'Control_L' then self.key_mods = key_mods | 4
         when 'Control_R' then self.key_mods = key_mods | 4
         when 'Alt_L' then self.key_mods = key_mods | 8
         when 'Alt_R' then self.key_mods = key_mods | 8
      end

      t_mods = key_mods
      e.keysym =~ /\AKP_/ and t_mods = t_mods | 1   # numlock on always

      #puts "mask: #{key_mods.to_s(2)}"
      #puts "keysym_num: #{e.keysym_num}"

      # cripes
      shift_chars.has_key?(e.char) or t_mods = t_mods & (8191^1)

      send_to_all_xterms(e.keysym_num,t_mods)
   end
   def handle_key_release(e)
      e.widget.clear
      #e.widget.clear
      #puts '-'*79
      #puts 'RELEASE'
      #puts e.inspect

      case e.keysym
         when 'Shift_L' then self.key_mods = key_mods & (8191^1)
         when 'Shift_R' then self.key_mods = key_mods & (8191^1)
         when 'Control_L' then self.key_mods = key_mods & (8191^4)
         when 'Control_R' then self.key_mods = key_mods & (8191^4)
         when 'Alt_L' then self.key_mods = key_mods & (8191^8)
         when 'Alt_R' then self.key_mods = key_mods & (8191^8)
      end

      #puts "mask: #{key_mods.to_s(2)}"
      #puts e.instance_variable_get('keysym')
      #xterms.each { |x| x.send_keyrelease(e.keysym_num) }
   end
   def shift_chars
      @shift_chars ||= Hash[
         SHIFT_CHARS.zip(SHIFT_CHARS.count.times.collect { true })
      ]
   end
   def handle_button_2_release(e)
#puts TkClipboard.get('PRIMARY').inspect
      e.widget.text_paste # causes e.widget.value to be filled in
      t_value = e.widget.value
      puts "VALUE:#{t_value}"
      t_value.each_char { |c|
         #puts c.inspect

         case
            when c == "\r" then send_to_all_xterms(65293,0)
            when c == "\n" then send_to_all_xterms(65293,0)
         else
            t_mods = 0
            shift_chars.has_key?(c) and t_mods = 1
            send_to_all_xterms(c.ord,t_mods)
         end

      }
      #send_to_all_xterms(65293,0)
      e.widget.value = nil
      e.widget.clear
   end
   def handle_delete_window
         xterms.
            find_all { |x| !x.dead? }.
            each { |x| x.close }
         tkroot.destroy
         self.tkroot = nil
   end
   def send_to_all_xterms(i_keycode,i_mods)
      xterms.
         find_all { |x| x.dead? }.
         find_all { |x| !x.cleaned? }.
         each { |x| x.cleanup! }

      xterms.
         find_all { |x| !x.dead? }.
         each { |x| x.send_keypress(i_keycode,i_mods) }
   end
end




