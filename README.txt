
Mutiplex keystrokes to multiple XTerm windows.

A partial duplication of the clusterssh project's functionality, except in Ruby, and without a particular bug affecting Plasma 5 (windows lose ability to move or resize) (probably a Plasma 5 bug, but they won't admit it) (in writing this I did not duplicate the bug).

It's ultimately a library that you use in Ruby code -- using Ruby as a DSL for configuring options for the multiple windows.

You need ffi and tk installed.  And you need gcc as well.

Admittedly, my handling of XLib key-codes is half-assed, and/but works for me -- uhhh, I wouldn't expect it to work for everyone yet -- sorry.


INSTALL

cd ext
make


EXAMPLES

provided: bin/clustered_xterm_example.rb

A bunch of hosts:

ClusteredXTerm.new(
   {command:'ssh vm-gentoo'},
   {command:'ssh hadoop-a1'},
   {command:'ssh hadoop-a2'},
   {command:'ssh hadoop-a3'},
   {command:'ssh hadoop-a4'},
   {command:'ssh hadoop-b1'},
   {command:'ssh hadoop-b2'},
   {command:'ssh hadoop-b3'},
   {command:'ssh hadoop-b4'},
   {command:'ssh hadoop-c1'},
   {command:'ssh hadoop-c2'},
   {command:'ssh hadoop-c3'},
   {command:'ssh hadoop-c4'},
   i_global: { }
).run


With settings for background and foreground colors:

ClusteredXTerm.new(
   {bg:'lightyellow',command:'ssh moksha'},
   {bg:'lightyellow',command:'ssh anglachel'},
   {bg:'lightyellow'},
   i_global: { fg:'black' }
).run



